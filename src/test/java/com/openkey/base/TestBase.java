package com.openkey.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.openkey.util.Xls_Reader;

public class TestBase {
	public static Logger APP_LOGS=null;
	public static Properties CONFIG=null;
	public static Properties OR=null;
	public static Xls_Reader suiteXls=null;
	public static Xls_Reader suiteAxls=null;
	public static Xls_Reader suiteBxls=null;
	public static Xls_Reader suiteCxls=null;
	public static boolean isInitalized=false;
	public static WebDriver driver=null;
	
	
	// initializing the Tests
	public void initialize() throws Exception{
		// logs
		if(!isInitalized){
		APP_LOGS = Logger.getLogger("devpinoyLogger");
		// config
		APP_LOGS.debug("Loading Property files");
		CONFIG = new Properties();
		FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+"//src//test//java//com//openkey//config//config.properties");
		CONFIG.load(ip);
		OR = new Properties();
		ip = new FileInputStream(System.getProperty("user.dir")+"//src//test//java//com//openkey//config//OR.properties");
		OR.load(ip);
		APP_LOGS.debug("Loaded Property files successfully");
		APP_LOGS.debug("Loading XLS Files");

		// xls file
		suiteAxls = new Xls_Reader(System.getProperty("user.dir")+"//src//test//java//com//openkey//xls//A suite.xlsx");
		suiteXls = new Xls_Reader(System.getProperty("user.dir")+"\\src\\test\\java\\com\\openkey\\xls\\Suite.xlsx");
		APP_LOGS.debug("Loaded XLS Files successfully");
		isInitalized=true;
		}
		// selenium RC/ Webdriver
		OpenApp(CONFIG.getProperty("browserType"),CONFIG.getProperty("SiteName"));
	}
		 
		// Initialize browser
		public static WebDriver OpenApp(String BrowserName, String url){
			fn_LaunchBrowser(BrowserName);
			fn_OpenURL(url);
			return driver;
			}
			public static void fn_OpenURL(String url){
			driver.get(url);
			driver.manage().window().maximize();
			}
			 
			public static WebDriver fn_LaunchBrowser(String browsername){
			if(browsername.equalsIgnoreCase("CH")){
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
			driver= new ChromeDriver();
			}else if(browsername.equalsIgnoreCase("FF")){
            driver= new FirefoxDriver();
            }else if(browsername=="IE"){
            System.setProperty("webdriver.ie.driver", "Drivers\\IEDriverServer.exe");
            driver= new InternetExplorerDriver();
			}
			driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			return driver;
			
			}
}

		
		
		
	


